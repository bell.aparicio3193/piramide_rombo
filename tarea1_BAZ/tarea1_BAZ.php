<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Figuras</title>
</head>
<body>
  <center>
    <h2>Piramide</h2>
    <?php
      //Trae el valor de lo que escribio el usuario en el formulario
      $num = $_POST['numFilas'];
      // Vertical
      for ($i=1; $i <=$num; $i++) { 
        //Horizontal
        for ($j=1; $j <=$i ; $j++) { 
          echo "* ";
        }
        echo "<br>";
      }
    ?>
    <hr>
    <h2>Rombo</h2>
    <?php
      // Rombo
      for ($i=1; $i <=$num; $i++) { 
        for ($j=1; $j <=$i ; $j++) { 
          echo "* ";
        }
        echo "<br>";
      }
      for ($i=1; $i <=$num; $i++) { 
        for ($j=$num; $j>=$i ; $j--) { 
          echo "* ";
        }
        echo "<br>";
      }
    ?>
  </center>
</body>
</html>